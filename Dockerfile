# Building stage
FROM node:12-alpine as build-stage

RUN npm install -g http-server
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --production
COPY . .

RUN mkdir /config \
    && rm /usr/src/app/src/config/config.json \
    && ln -s /config/config.json /usr/src/app/src/config/config.json

VOLUME /config

RUN npm run build

# Production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /usr/src/app/dist /usr/share/nginx/html
EXPOSE 80

CMD [ "nginx", "-g", "daemon off" ]