# Vigil - Frontend

Frontend part of the Vigil application used to monitor services.

## Installation

### Prerequesites

You need **Node.JS** alongside **Vue CLI** to install and use the application.

### Installation Procedure

You need to create a config file in `./config`container containing different configuration information for the server. It should have the corresponding structure :

```json
config.json
---

{
    "API_BACKEND": "http://localhost:4000"
}

```

## Usage

### Docker

A Docker container is available at pierrehugues/vigil-frontend. It uses a volume called /config to set the configuration for the different parts of the application.

3 images are available :

- `:master`: latest successfull push on master
- `:latest`: latest successfull tag
- `:v.x.y.z`: specific version

Launch the Docker container using :

```shell
docker run -d --name vigil_frontend --net=host --restart always -v "$(pwd)/config":/config pierrehugues/vigil-frontend:latest
```

### Running the application

You can run the application using the `npm run build` command and Apache or NGINX to expose your application.
