import axios from "axios";
import config from "../config/config";

const API_URL = config.API_BACKEND;

export default class ServicesAPI {

    /**
     * Call the API to get the list of services
     */
    getServices() {
        const url = `${API_URL}/services`;
        const token = JSON.parse(localStorage.getItem("token")).value;
        return axios.get(url, {
            headers: { Authorization: token },
        }).then(response => response.data);
    }

    /**
     * Call the API to get the data of a service
     * @param {Object} service service to get
     */
    getService(service) {
        const url = `${API_URL}/services/${service._id}`;
        const token = JSON.parse(localStorage.getItem("token")).value;
        return axios.get(url, {
            headers: { Authorization: token },
        }).then(response => response.data);

    }

    /**
     * Call the API to create a service
     * @param {Object} service service to create
     */
    createService(service) {
        const url = `${API_URL}/service`;
        const token = JSON.parse(localStorage.getItem("token")).value;
        return axios.post(url, service, {
            headers: { Authorization: token },
        });
    }

    /**
     * Call the API to create services based on a csv file
     * @param {File} csv file to import
     */
    importServiceCSV(csv) {
        const url = `${API_URL}/services/csv`;
        const token = JSON.parse(localStorage.getItem("token")).value;
        return axios.post(url, csv, {
            headers: { Authorization: token },
        });
    }

    /** 
     * Call the API to delete a service
     * @param {Object} service service to delete
     */
    deleteService(service) {
        const url = `${API_URL}/services/${service._id}`;
        const token = JSON.parse(localStorage.getItem("token")).value;
        return axios.delete(url, {
            headers: { Authorization: token },
        });
    }

    /**
     * Call the API to update a service
     * @param {Object} service service to update
     */
    updateService(service) {
        const url = `${API_URL}/services/${service._id}`;
        const token = JSON.parse(localStorage.getItem("token")).value;
        return axios.put(url, service, {
            headers: { Authorization: token },
        });
    }

    /**
     * Call the API to find entries using a nmap script
     * @param {String} Adress Adress to check with nmap
     */
    importNMAP(adress) {
        const url = `${API_URL}/services/nmap`;
        const token = JSON.parse(localStorage.getItem("token")).value;
        return axios.post(url, adress, {
            headers: { Authorization: token },
        });
    }

}
