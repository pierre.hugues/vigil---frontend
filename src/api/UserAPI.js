import axios from "axios";
import config from "../config/config";

const API_URL = config.API_BACKEND;

export default class UserAPI {

    /**
     * Call the API to get the list of users
     */
    getUsers() {
        const url = `${API_URL}/users`;
        return axios.get(url).then(response => response.data);
    }

    /**
     * Call the API to create a user
     * @param {Object} user User to create
     */
    createUser(user) {
        const url = `${API_URL}/signup`;
        return axios.post(url, user);
    }

    /**
     * Call the API to verify a user
     * @param {Number} id ID of the user
     */
    verifUser(id) {
        const url = `${API_URL}/verif/${id}`;
        return axios.get(url).then(response => response.data);
    }

    /**
     * Call the API to resend a mail to confirm an account
     * @param {Number} id ID of the user
     */
    resendMail(id) {
        const url = `${API_URL}/resendMail/${id}`;
        return axios.get(url).then(response => response.data);
    }

    /**
     * Call the API to log
     * @param {Object} user User to log
     */
    login(user) {
        const url = `${API_URL}/login`;
        return axios.post(url, user);
    }

    /**
     * Check if the token is leggit
     * @param {String} token Token to verify
     */
    checkToken(token) {
        const url = `${API_URL}/token/${token}`;
        return axios.get(url).then(response => response.data);
    }

    /**
     * Update a user
     * @param {Object} user User to update
     */
    updateUser(user) {
        const url = `${API_URL}/user/${user._id}`;
        const token = JSON.parse(localStorage.getItem("token")).value;
        return axios.put(url, user, {
            headers: { Authorization: token },
        });
    }

    /**
     * Send a recuperation link to a user who forgot his password
     * @param {Object} user User who forgot his mail
     */
    forgottenPwd(user) {
        const url = `${API_URL}/forgottenPwd`;
        const token = JSON.parse(localStorage.getItem("token")).value;
        return axios.post(url, user, {
            headers: { Authorization: token },
        });
    }

}