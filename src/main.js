import Vue from "vue";
import App from "./views/App.vue";
import i18n from "./i18n";
import vuetify from "./plugins/vuetify";
import router from "./router";

import "@fortawesome/fontawesome-free/css/all.css";
import VueNoty from "vuejs-noty";
import "vuejs-noty/dist/vuejs-noty.css";

Vue.use(VueNoty);


Vue.config.productionTip = false;

new Vue({
    i18n,
    vuetify,
    router,
    render: h => h(App),
}).$mount("#app");
