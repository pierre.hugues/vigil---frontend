import Vue from "vue";
import Router from "vue-router";

import Services from "@/views/services/Services.vue";

import Login from "@/views/login/Login.vue";
import ResetPwd from "@/views/login/ResetPwd.vue";
import ResetConfirmation from "@/views/login/ResetConfirmation.vue";
import ForgetPwd from "@/views/login/ForgetPwd.vue";

import Signup from "@/views/signup/Signup.vue";
import Verify from "@/views/signup/Verify.vue";
import Confirm from "@/views/signup/Confirm.vue";

import CGU from "@/views/help/CGU.vue";

import User from "@/views/user/User.vue";

import APIUser from "@/api/UserAPI.js";

Vue.use(Router);

const router = new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            redirect: "/services",
        },
        {
            path: "*",
            redirect: "/",
        },
        {
            path: "/services",
            name: "Services",
            component: Services,
            meta: {
                requiresAuth: true,
            },
        },
        {
            path: "/services",
            name: "List Services",
            component: Services,
            meta: {
                requiresAuth: true,
            },
        },
        {
            path: "/user",
            name: "User",
            component: User,
            meta: {
                requiresAuth: true,
            },
        },
        {
            path: "/login",
            name: "Login",
            component: Login,
        },
        {
            path: "/forgetPwd",
            name: "ForgetPwd",
            component: ForgetPwd,
        },
        {
            path:"/resetPwd",
            name: "ResetPwd",
            component: ResetPwd,
        },
        {
            path: "/forgetPwd/reset",
            name: "ResetPwd",
            component: ResetPwd,
        },
        {
            path: "/resetConfirmation",
            name: "ResetConfirmation",
            component: ResetConfirmation,
        },
        {
            path: "/signup",
            name: "Signup",
            component: Signup,
        },
        {
            path: "/help/conditions_of_use",
            name: "Conditions of Use",
            component: CGU,
        },
        {
            path: "/verify/:id",
            name: "Verify User",
            component: Verify,
        },
        {
            path: "/confirmAccount/:id",
            name: "Confirm account",
            component: Confirm,
        },
    ],
});

router.beforeEach(async (to, from, next) => {
    try {
        if(to.matched.some(record => record.meta.requiresAuth)) {
            const apiUser = new APIUser();
            const data = localStorage.getItem("token");
            if(!data) {
                next({name: "Login"});
                return;
            }
            let { value, expiry } = JSON.parse(data);
            if(new Date().getTime() > expiry) {
                localStorage.removeItem("token");
                next({name: "Login"});
                return;
            }
            await apiUser.checkToken(value);
            next();
        } else {
            next();
        }
    } catch (error) {
        console.error(error);
        next({name: "Login"});
    }
});

export default router;
